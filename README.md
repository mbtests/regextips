# RegexTips

all characters BEFORE pattern (pattern example: ":"):
```
.*(?<=:)
```

all characters AFTER LAST OCCURENCE of a pattern (pattern example: "/"):
```
[^\\/]*$
```

square brackets ([]) and their content (TRE):
```
\\[[^][]*]
```

angle brackets (<>) and their content (TRE):
```
<[^>]*>
```